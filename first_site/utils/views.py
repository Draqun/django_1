from django.http import HttpResponse
from django.urls import reverse
from django.views import View
from django.views.generic import TemplateView


class IndexView(View):
    def get(self, request):
        bindex = reverse("bindex")
        about = reverse("about")
        return HttpResponse("\
        Welcome in Django World! Go to <a href='{}'>Bootstrap example</a>\
        or go to <a href='{}'>About</a>\
        ".format(bindex, about))


index_view = IndexView.as_view()


class AboutView(TemplateView):
    template_name = "about.html"


about_view = AboutView.as_view()
