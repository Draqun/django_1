from django.test import TestCase
from django.urls import reverse


class BIndexViewTests(TestCase):
    def test_page_is_loaded(self):
        response = self.client.get(reverse('bindex'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Welcome in Django-Bootstrap world! :-)")
        self.assertContains(response, "Overloaded footer place")
