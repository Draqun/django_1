from django.apps import AppConfig


class BootstrapExampleConfig(AppConfig):
    name = 'bootstrap_example'
